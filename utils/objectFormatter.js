/**
 * Returns a new object without the fields described in the field parameter
 * @param {Object} obj The object to format
 * @param {[String]} fields An array of the fields to delete (optional, delete _id by default)
 */
function format(obj, fields = ["_id"]) {
  let newObj = obj.toObject();
  for (const field of fields) {
    delete newObj[field];
  }
  return newObj;
}

/**
 * Return the user object without the _id, idAdmin and tempEmailToken fields
 * @param {Object} obj The user to format
 */
function formatUser(obj) {
  const fields = ["isAdmin", "tempEmailToken"];
  return format(obj, fields);
}

/**
 * Return the user object without the _id, password, idAdmin, email, tempEmail and tempEmailToken fields
 * The remaining fields are the ones visibles by any user
 * @param {Object} obj The user to format
 */
function formatUserPublic(obj) {
  const fields = ["_id", "password", "isAdmin", "email", "tempEmail", "tempEmailToken", "tempPasswordToken"];
  return format(obj, fields);
}

module.exports = {
  formatUser,
  formatUserPublic,
};
