const jwt = require("jsonwebtoken");
const USER_TOKEN_SECRET = process.env.USER_TOKEN_SECRET;

/**
 * Checks the auth-token header in the request sent
 * If there is no auth-token, return unauthorized
 * If the auth-token is wrong, return forbidden
 * If the auth-token is legit, the user is extracted from the header and stored in req.user
 */
const authToken = (req, res, next) => {
  const authToken = req.header("auth-token");
  if (!authToken) return res.status(401).json({ error: "Please authenticate yourself first" });
  try {
    const verified = jwt.verify(authToken, USER_TOKEN_SECRET);
    req.user = verified;
    next();
  } catch (e) {
    // If the token is falsified, return unauthorized
    // (Debate on status 403)
    return res.status(401).json({ error: "Access forbidden - wrong auth-token" });
  }
};

module.exports = { authToken };
