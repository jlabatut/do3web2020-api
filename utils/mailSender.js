const nodemailer = require("nodemailer");

module.exports = async (to, data) => {
  if (data.type == "resetPassword") {
    data.subject = "Réinitialisation de votre mot de passe";
    const resetPasswordEmail = require("./mails/resetPasswordEmail");
    data.text = resetPasswordEmail(data);
  } else if (data.type == "editMail") {
    data.subject = "Mise à jour de votre adresse mail";
    const editEmail = require("./editEmail");
    data.text = editEmail(data);
  } else {
    console.log("error, email type not found");
    return 1;
  }
  const transporter = nodemailer.createTransport({
    service: "gmail",
    auth: {
      user: process.env.MAIL_USERNAME,
      pass: process.env.MAIL_PASSWORD,
    },
  });

  let mailOptions = {
    from: "Coding Ideas <" + process.env.MAIL_USERNAME + ">",
    to: to,
    subject: data.subject,
    html: data.text,
  };
  try {
    await transporter.sendMail(mailOptions);
  } catch (e) {
    console.log("sendMail error : ", e);
  }
};
