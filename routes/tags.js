import { Router } from "express";
const { authToken } = require("../utils/tokenCheck");
const ObjectId = require("mongoose").Types.ObjectId;

import Tag from "../models/tag";
import User from "../models/user";

const router = Router();

/**
 * If req.user is admin, returns all tags with
 * If req.user is someone else, returns unauthorized (403)
 */
router.get("/", authToken, async (req, res) => {
  try {
    if (req.user.isAdmin) {
      let tags = await Tag.find();
      return res.json({ tags });
    } else {
      let tags = await Tag.find({}, "title");
      return res.json({ tags });
    }
  } catch (e) {
    return res.status(500).json({ error: `Server error : ${e.message}` });
  }
});

/**
 * If req.user is admin, returns all tags from an author
 * If req.user is someone else, returns unauthorized (403)
 */
router.get("/user/:username", authToken, async (req, res) => {
  if (req.user.isAdmin) {
    try {
      let user = await User.findOne({ username: req.params.username });
      if (user == null) {
        return res.status(404).json({ error: "User not found" });
      }
      let tags = await Tag.find({ author: user.id });
      return res.json({ tags });
    } catch (e) {
      return res.status(500).json({ error: `Server error : ${e.message}` });
    }
  } else {
    return res.status(403).json({ error: "Access forbidden" });
  }
});

/**
 * Returns the tag :tag
 */
router.get("/:tag", getTag, async (req, res) => {
  return res.json({ tag: res.tag });
});

/**
 * Creates a new tag with the user authenticated as the author
 */
router.post("/", authToken, async (req, res) => {
  if (!req.body.title) {
    return res.status(400).json({ error: "wrong format : need json with title field" });
  }
  try {
    // let tagExists = await Tag.find({ title: { $regex: req.body.title, $options: "i" } }, { author: 0, _id: 0 });
    // if (tagExists) {
    //   for (let i = 0; i < tagExists.length; i++) tagExists[i] = tagExists[i].title;
    //   console.log(tagExists, tagExists.indexOf(new RegExp(req.body.title, "i")));
    //   if (tagExists.indexOf(new RegExp(req.body.title)) > -1)
    //     return res.status(404).json({ error: "The tag already exists" });
    // }
    let tagExists = await Tag.find({ title: req.body.title }, { author: 0, _id: 0 });
    if (tagExists.length > 0) return res.status(409).json({ error: "The tag already exists" });
    let author = req.user.isAdmin ? (req.body.author ? req.body.author : null) : req.user.id;
    let tag = new Tag({
      title: req.body.title,
      ...(author && { author }),
    });
    let createdTag = await Tag.create(tag);
    return res.status(201).json({ tag: createdTag });
  } catch (e) {
    return res.status(500).json({ error: `Server error : ${e.message}` });
  }
});

/**
 * If req.user is admin, patches the tag :tag on updated fields and returns the tag with the new values
 * If req.user is someone else, returns unauthorized (403)
 */
router.patch("/:tag", authToken, getTag, async (req, res) => {
  if (req.user.isAdmin) {
    try {
      if (req.body.title) res.tag.set("title", req.body.title);
      if (req.body.author) res.tag.set("author", req.body.author);
      if (req.body.author == "") res.tag.set("author", undefined);
      const updatedTag = await res.tag.save();
      res.json(updatedTag);
    } catch (e) {
      return res.status(500).json({ error: `Server error : ${e.message}` });
    }
  } else {
    return res.status(403).json({ error: "Access forbidden" });
  }
});

/**
 * If req.user is admin , delete the tag and return nothing (204)
 * If req.user is someone else, returns unauthorized (403)
 */
router.delete("/:tag", authToken, getTag, async (req, res) => {
  if (req.user.isAdmin) {
    try {
      await res.tag.deleteOne();
      return res.status(204).json();
    } catch (e) {
      return res.status(500).json({ error: `Server error : ${e.message}` });
    }
  } else {
    return res.status(403).json({ error: "Access forbidden" });
  }
});

/**
 * Gets and returns a tag by his id
 * If the tag is not stored in the DB, returns 404 Not found
 */
export async function getTag(req, res, next) {
  let tag;
  try {
    if (!ObjectId.isValid(req.params.tag)) return res.status(404).json({ error: "Tag not found | Wrong format" });
    tag = await Tag.findById(req.params.tag);
    // .populate([
    //   { path: "subscribers", select: { username: 1 } },
    //   { path: "relatedPosts", select: { title: 1 } },
    // ]);
    if (tag == null) {
      return res.status(404).json({ error: "Tag not found" });
    }
  } catch (e) {
    return res.status(500).json({ error: `Server error : ${e.message}` });
  }
  res.tag = tag;
  next();
}

export default router;
