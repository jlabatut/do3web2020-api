import { Router } from "express";
const { authToken } = require("../utils/tokenCheck");
const ObjectId = require("mongoose").Types.ObjectId;

import Comment from "../models/comment";
import Post from "../models/post";
import User from "../models/user";
// import {} from "../utils/objectFormatter";

const router = Router();

/**
 * If req.user is admin, returns all comments
 * If req.user is someone else, returns unauthorized (403)
 */
router.get("/", authToken, async (req, res) => {
  if (req.user.isAdmin) {
    try {
      let comments = await Comment.find();
      return res.json({ comments: comments });
    } catch (e) {
      return res.status(500).json({ error: `Server error : ${e.message}` });
    }
  } else {
    return res.status(403).json({ error: "Access forbidden" });
  }
});

/**
 * Returns all comments from an author
 */
router.get("/user/:userId", authToken, async (req, res) => {
  try {
    let user = await User.findOne({ username: req.params.userId });
    if (!user) {
      if (!ObjectId.isValid(req.params.userId)) return res.status(404).json({ error: "User not found" });
      user = await User.findById(req.params.userId);
    }
    if (user == null) {
      return res.status(404).json({ error: "User not found" });
    }
    let comments = await Comment.find({ author: user.id });
    return res.json({ comments });
  } catch (e) {
    return res.status(500).json({ error: `Server error : ${e.message}` });
  }
});

/**
 * Returns all comments related to a Post
 */
router.get("/post/:originalPost", authToken, async (req, res) => {
  if (!ObjectId.isValid(req.params.originalPost))
    return res.status(404).json({ error: "The original Post does not exists (not valid ID)" });
  try {
    let post = await Post.findById(req.params.originalPost);
    if (post == null) {
      return res.status(404).json({ error: "Original Post not found" });
    }
    let comments = await Comment.find({ originalPost: req.params.originalPost }).populate([
      { path: "author", select: { username: 1 } },
    ]);
    return res.json({ comments });
  } catch (e) {
    return res.status(500).json({ error: `Server error : ${e.message}` });
  }
});

/**
 * Returns the comment :comment
 */
router.get("/:comment", getComment, async (req, res) => {
  //   console.log(await getPath(res.comment._id));
  return res.json({ comment: res.comment });
});

/**
 * Creates a new comment with the user authenticated as the author
 */
router.post("/", authToken, async (req, res) => {
  if (!req.body.originalPost || !req.body.content) {
    return res.status(400).json({ error: "wrong format : need json with originalPost and content fields" });
  }
  if (!ObjectId.isValid(req.body.originalPost))
    return res.status(404).json({ error: "The originalPost does not exists (not valid ID)" });
  try {
    let originalPost = await Post.findById(req.body.originalPost);
    if (!originalPost) return res.status(404).json({ error: "The originalPost does not exists" });
    let comment = new Comment({
      originalPost: req.body.originalPost,
      author: req.user.id,
      content: req.body.content,
    });
    let createdComment = await comment.save();
    // let createdComment = await Comment.create(comment);
    createdComment = await createdComment.populate([{ path: "author", select: { username: 1 } }]).execPopulate();
    return res.status(201).json({ comment: createdComment });
  } catch (e) {
    return res.status(500).json({ error: `Server error : ${e.message}` });
  }
});

/**
 * If req.user is admin or the owner of :comment, patches the comment :comment on updated fields and returns the comment with the new values
 *    If the value in the body is null or empty, it deletes the field in the DB
 *    If the value is different from the current one in the DB, it updates the field with the new value
 *    If the value is the same as the one in the DB, or if the field is not part of the body, the field is not updated
 * If req.user is someone else, returns unauthorized (403)
 */
router.patch("/:comment", authToken, getComment, async (req, res) => {
  if (req.user.isAdmin || res.comment.author == req.user.id) {
    try {
      // For each field, checks if it is different from the current value and is not empty
      for (let field in req.body) {
        let newValue = req.body[field];
        try {
          // Only an admin can edit a comment author
          if (((field == "author" || field == "originalPost") && !req.user.isAdmin) || field == "_id") {
            continue;
          }
          // If the new value is null, the field is deleted in the DB to save storage space
          if (newValue == "" || newValue == "undefined" || newValue == null) {
            if (!(field == "originalPost" || field == "author")) {
              res.comment.set(field, undefined);
            }
          } else if (res.comment.get(field) !== newValue) {
            if (!(field == "originalPost" || field == "author")) {
              res.comment.set(field, newValue);
            }
          }
        } catch (e) {
          console.log(e);
          return res.status(400).json({ error: e.message });
        }
      }
      const updatedComment = await res.comment.save();
      res.json(updatedComment);
    } catch (e) {
      return res.status(500).json({ error: `Server error : ${e.message}` });
    }
  } else {
    return res.status(403).json({ error: "Access forbidden" });
  }
});

/**
 * If req.user is admin or the owner of the comment :comment, delete the comment and return nothing (204)
 * If req.user is someone else, returns unauthorized (403)
 */
router.delete("/:comment", authToken, getComment, async (req, res) => {
  if (req.user.isAdmin || res.comment.author == req.user.id) {
    try {
      await res.comment.deleteOne();
      return res.status(204).json();
    } catch (e) {
      return res.status(500).json({ error: `Server error : ${e.message}` });
    }
  } else {
    return res.status(403).json({ error: "Access forbidden" });
  }
});

/**
 * Gets and returns a comment by his id
 * If the comment is not stored in the DB, returns 404 Not found
 */
async function getComment(req, res, next) {
  let comment;
  try {
    if (!ObjectId.isValid(req.params.comment)) return res.status(404).json({ error: "Comment not found" });
    comment = await Comment.findById(req.params.comment);
    if (comment == null) {
      return res.status(404).json({ error: "Comment not found" });
    }
  } catch (e) {
    return res.status(500).json({ error: `Server error : ${e.message}` });
  }
  // comment._doc = { ...comment._doc, path: await getPath(comment._id) };
  res.comment = comment;
  next();
}

/**
 * USELESS/OBSOLETE
 * Returns an array with all the previous comments and the original post
 * Usage : await getPath(res.comment._id)
 * @param {String} id The comment id for which you want the previous comments
 */
async function getPath(id) {
  let path = [];
  try {
    while (true) {
      let previousObject = (await Post.findById(id)) || (await Comment.findById(id));
      let previous = previousObject.previous;
      //   console.log(previousObject.content, "is", id, ", previous is", previous);
      if (!previous) {
        break;
      }
      path.push(previous);
      id = previous;
    }
  } catch (e) {
    console.log(e.message);
    return res.status(500).json({ error: `Server error : ${e.message}` });
  }
  return path.reverse();
}

export default router;
