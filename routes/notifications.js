import { Router } from "express";
const { authToken } = require("../utils/tokenCheck");
const { getUser } = require("./users");

import Post from "../models/post";
import User from "../models/user";
import Notification from "../models/notification";

const router = Router();

router.get("/username/:username", authToken, getUser, async (req, res) => {
  if (req.user.isAdmin || req.params.username == req.user.username) {
    try {
      let notifications = await Notification.find({ target: res.user.id }).populate([
        { path: "tags", select: { title: 1 } },
      ]);
      return res.json({ notifications });
    } catch (e) {
      return res.status(500).json({ error: `Server error : ${e.message}` });
    }
  } else {
    return res.status(403).json({ error: "Access forbidden" });
  }
});

async function createNotification(target, content, title = "", link = "", sender = "", tags = []) {
  try {
    const newNotif = new Notification({
      target,
      content,
      ...(title.length > 0 && { title: title }),
      ...(link.length > 0 && { link: link }),
      ...(sender.length > 0 && { sender: sender }),
      ...(tags.length > 0 && { tags: tags }),
    });
    await Notification.create(newNotif);
  } catch (e) {
    console.error("Error when create notif: ", e);
  }
}

/**
 * Triggered when a new comment is submited.
 * Create a new notification related to all users subscribed to the original post,
 * and create a new notification for the author of the original post
 * @param {Document} comment The new comment
 */
export async function notifyOnNewComment(comment) {
  if (comment.createdAt === comment.updatedAt) {
    try {
      let OriginalPost = await Post.findOne(comment.originalPost, { subscribers: 1, author: 1, title: 1 });
      const promises = OriginalPost.subscribers.map((sub) => {
        if (sub.toString() !== OriginalPost.author.toString() && comment.author.toString() !== sub.toString()) {
          createNotification(
            sub,
            "Nouveau commentaire sur une idée que vous suivez.",
            OriginalPost.title,
            "/idea/" + OriginalPost._id
          );
        }
      });
      if (comment.author.toString() !== OriginalPost.author.toString()) {
        await createNotification(
          OriginalPost.author,
          "Nouveau commentaire sur votre idée.",
          OriginalPost.title,
          "/idea/" + OriginalPost._id
        );
      }
      await Promise.all(promises);
    } catch (e) {
      console.log("Error on create comment notification : ", e);
    }
  }
}

/**
 * Triggered when a user post a new idea.
 * Create notification for all users subscribed to tags included in the new post
 * @param {Document} post The new post
 */
export async function notifyOnNewPost(post) {
  if (post.tags && post.tags.length > 0) {
    try {
      // Create notification for all subscribed users
      const subscribedUsers = await User.find({ tags: { $in: post.tags } });
      const promises = subscribedUsers.map((sub) => {
        if (post.author.toString() !== sub._id.toString()) {
          let tags = sub.tags.filter((someTag) => post.tags.includes(someTag));
          createNotification(
            sub._id,
            `Nouvelle idée qui pourrait peut-être vous intéresser car vous suivez ${
              tags.length > 1 ? "ces tags" : "ce tag"
            } :`,
            post.title,
            "/idea/" + post._id,
            "",
            tags
          );
        }
      });
      await Promise.all(promises);
      // await createNotification();
    } catch (e) {
      console.error(e);
    }
  }
}

export default router;
