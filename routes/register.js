import express from "express";
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

import User from "../models/user";
import { regexEmail, regexUsername, regexPassword } from "../utils/constants";

const router = express.Router();

// Register : vérification des données rentrées, vérification de non existence de l'username/email puis création de l'user
router.post("/", async (req, res) => {
  if (!req.body.username || !req.body.email || !req.body.password)
    return res.status(400).json({ error: "wrong format : need json with username, email and password fields" });
  if (
    !req.body.username.match(regexUsername) ||
    !req.body.email.match(regexEmail) ||
    !req.body.password.match(regexPassword)
  )
    return res.status(400).json({ error: "wrong data : regex not matched with username, email or password fields" });

  try {
    // Vérification de l'existence d'un utilisateur similaire
    const emailExists = await User.findOne({ email: req.body.email });
    if (emailExists) return res.status(409).json({ error: "L'email est déjà utilisé par un compte existant." });
    const usernameExists = await User.findOne({ username: req.body.username });
    if (usernameExists) return res.status(409).json({ error: "Le nom d'utilisateur est déjà pris." });

    // Hash du password
    const salt = await bcrypt.genSalt(10);
    const hashedPassword = await bcrypt.hash(req.body.password, salt);

    // Création de l'objet User à stocker en base
    const user = new User({ username: req.body.username, password: hashedPassword, email: req.body.email });
    await user.save();

    // Création du token de connexion
    const token = jwt.sign(
      { id: user.id, username: user.username, isAdmin: user.isAdmin },
      process.env.USER_TOKEN_SECRET
    );
    return res.status(201).json({ user: user, token });
  } catch (e) {
    return res.status(500).json({ error: e.message });
  }
});

export default router;
