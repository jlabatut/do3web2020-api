import express from "express";
const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

import User from "../models/user";
import { regexEmail, regexUsername, regexPassword } from "../utils/constants";
import { formatUser } from "../utils/objectFormatter";
import makeId from "../utils/randomIdGenerator";
const sendEmail = require("../utils/mailSender");

const router = express.Router();

// Login : vérification des données puis création du token de login user
router.post("/", async (req, res) => {
  if (
    !req.body.username ||
    !req.body.password ||
    typeof req.body.username !== "string" ||
    typeof req.body.password !== "string"
  )
    return res.status(400).json({ error: "wrong format : need json with username and password fields" });
  if (
    (!req.body.username.match(regexUsername) && !req.body.username.match(regexEmail)) ||
    !req.body.password.match(regexPassword)
  )
    return res.status(400).json({ error: "wrong data : regex not matched with username, email or password fields" });

  try {
    // Vérification de l'existence de l'user (en fonction du nom ou de l'email)
    const user =
      (await User.findOne({ username: req.body.username })) || (await User.findOne({ email: req.body.username }));
    if (!user) return res.status(401).json({ error: "Le nom d'utilisateur ou le mot de passe sont incorrects" });

    // Vérification du mdp
    const validPass = await bcrypt.compare(req.body.password, user.password);
    if (!validPass) return res.status(401).json({ error: "Le nom d'utilisateur ou le mot de passe sont incorrects" });

    // Création du token de connexion
    const token = jwt.sign(
      { id: user.id, username: user.username, isAdmin: user.isAdmin },
      process.env.USER_TOKEN_SECRET
    );
    return res.json({ user: user, token });
  } catch (e) {
    console.log("login error: ", e);
    return res.status(500).json({ error: e.message });
  }
});

/**
 * Sends a reset password email to req.body.email
 * If the email provided is not in the DB, returns 404
 */
router.post("/forgottenPassword", async (req, res) => {
  if (!req.body.email || !req.body.email.match(regexEmail))
    return res.status(400).json({ error: "Need json with correct email field" });
  try {
    let user = await User.findOne({ email: req.body.email });
    if (!user) return res.status(404).json({ error: "Email not found" });
    let link = makeId(30);
    user.set("tempPasswordToken", link);
    await user.save();
    await sendEmail(user.email, { username: user.username, link, type: "resetPassword" });
    return res.json({ data: "Email sent to " + user.email });
  } catch (e) {
    return res.status(500).json({ error: e });
  }
});

/**
 * Check if the token provided in the URI is valid
 * If it does not match with any of users in the DB, returns 404
 */
router.get("/forgottenPassword/:token", async (req, res) => {
  try {
    let user = await User.findOne({ tempPasswordToken: req.params.token });
    if (!user) return res.status(404).json({ error: "Reset password token not found" });
    return res.json({ user: formatUser(user) });
  } catch (e) {
    return res.status(500).json({ error: e });
  }
});

/**
 * Updates the password of the user related to the token provided in the URI.
 * Then, clears the tempPasswordToken field stored in the user object.
 * If the token does not exists, returns 404.
 */
router.post("/forgottenPassword/:token", async (req, res) => {
  try {
    let user = await User.findOne({ tempPasswordToken: req.params.token });
    if (!user) return res.status(404).json({ error: "Reset password token not found" });
    if (!req.body.password || !req.body.password.match(regexPassword))
      return res.status(400).json({ error: "Wrong format: need correct password field" });
    const hashedPassword = await bcrypt.hash(req.body.password, await bcrypt.genSalt(10));
    user.set("password", hashedPassword);
    user.set("tempPasswordToken", undefined);
    await user.save();
    return res.json({ user: formatUser(user) });
  } catch (e) {
    return res.status(500).json({ error: e });
  }
});

export default router;
