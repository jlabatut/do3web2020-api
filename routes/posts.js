import { Router } from "express";
var ObjectId = require("mongoose").Types.ObjectId;
const { authToken } = require("../utils/tokenCheck");

import Post from "../models/post";
import User from "../models/user";
import Tag from "../models/tag";
import Comment from "../models/comment";
import { getTag } from "./tags";
// import {} from "../utils/objectFormatter";

const router = Router();

/**
 * If req.user is admin, returns all posts
 * If req.user is someone else, returns unauthorized (403)
 */
router.get("/", authToken, async (req, res) => {
  // if (req.user.isAdmin) {
  try {
    let posts = await Post.find()
      .populate([
        { path: "author", select: { username: 1 } },
        { path: "tags", select: { title: 1 } },
      ])
      .sort({ updatedAt: -1 });
    return res.json({ posts: posts });
  } catch (e) {
    return res.status(500).json({ error: `Server error : ${e.message}` });
  }
  // } else {
  //   return res.status(403).json({ error: "Access forbidden" });
  // }
});

router.get("/tags", authToken, async (req, res) => {
  let tags = [];
  if (!req.query.tag) return res.redirect("/posts");
  if (!Array.isArray(req.query.tag)) tags[0] = req.query.tag;
  else tags = req.query.tag;
  try {
    for (let tag of tags) {
      if (!ObjectId.isValid(tag)) return res.status(404).json({ error: `Tag ${tag} not found` });
    }
    res.json({
      posts: await Post.find({ tags: { $in: tags } })
        .populate([
          { path: "author", select: { username: 1 } },
          { path: "tags", select: { title: 1 } },
        ])
        .sort({ updatedAt: -1 }),
    });
  } catch (e) {
    res.status(500).json({ error: `Server error : ${e.message}` });
  }
});

/**
 * Returns all posts from an author
 */
router.get("/user/:username", async (req, res) => {
  try {
    let user = await User.findOne({ username: req.params.username });
    if (user == null) {
      return res.status(404).json({ error: "User not found" });
    }
    let posts = await Post.find({ author: user.id }).populate([
      { path: "author", select: { username: 1 } },
      { path: "tags", select: { title: 1 } },
    ]);
    return res.json({ posts });
  } catch (e) {
    return res.status(500).json({ error: `Server error : ${e.message}` });
  }
});

/**
 * Returns the post :post
 */
router.get("/:post", getPost, async (req, res) => {
  return res.json({ post: res.post });
});

/**
 * Creates a new post with the user authenticated as the author
 */
router.post("/", authToken, async (req, res) => {
  try {
    if (!req.body.title || !req.body.content) {
      return res.status(400).json({ error: "wrong format : need json with title and content fields" });
    }
    if (req.body.tags) {
      if (!Array.isArray(req.body.tags)) return res.status(400).json({ error: `tags field must be an array` });
      req.body.tags.forEach(async (e) => {
        if (!ObjectId.isValid(e)) return res.status(400).json({ error: `Tag ${e} is not ObjectId` });
        if (!(await Tag.findById(e))) return res.status(404).json({ error: `Tag ${e} does not exists` });
      });
    }
    let post = new Post({
      title: req.body.title,
      author: req.user.id,
      content: req.body.content,
      ...(req.body.tags && { tags: req.body.tags }),
    });
    let createdPost = await post.save();
    return res.status(201).json({ post: createdPost });
  } catch (e) {
    return res.status(500).json({ error: `Server error : ${e.message}` });
  }
});

/**
 * Useless route for now, may be deleted
 * Links or unlinks a post and a tag
 */
router.patch("/:post/tag/:tag", authToken, getPost, getTag, async (req, res) => {
  return res.status(503).json({ error: "Route is down for maintenance" });
  if (!req.body.action) return res.status(400).json({ error: "Need json with action field" });
  try {
    if (req.body.action === "subscribe") {
      if (res.post.tags.includes(res.tag.id) || res.post.tags.some((e) => e.id === res.tag.id))
        return res.status(409).json({ error: "Already subscribed" });
      res.post.tags.push(res.tag.id);
      res.post.save();
      // res.tag.relatedPosts.push(res.post.id);
      res.tag.save();
      return res.json({ status: "subscribed" });
    } else if (req.body.action === "unsubscribe") {
      if (!(res.post.tags.includes(res.tag.id) || res.post.tags.some((e) => e.id === res.tag.id)))
        return res.status(409).json({ error: "Already unsubscribed" });
      res.post.tags.pull(res.tag.id);
      res.post.save();
      // res.tag.relatedPosts.pull(res.post.id);
      res.tag.save();
      return res.json({ status: "unsubscribed" });
    } else return res.status(400).json({ error: "Action field must be 'subscribe' or 'unsubscribe'" });
  } catch (e) {
    console.log(e);
    return res.status(500).json({ error: `Server error : ${e.message}` });
  }
});

/**
 * If req.user is admin or the owner of :post, patches the post :post on updated fields and returns the post with the new values
 *    If the value in the body is null or empty, it deletes the field in the DB
 *    If the value is different from the current one in the DB, it updates the field with the new value
 *    If the value is the same as the one in the DB, or if the field is not part of the body, the field is not updated
 * If req.user is someone else, returns unauthorized (403)
 */
router.patch("/:post", authToken, getPost, async (req, res) => {
  if (req.user.isAdmin || res.post.author._id == req.user.id) {
    try {
      // For each field, checks if it is different from the current value and is not empty
      for (let field in req.body) {
        let newValue = req.body[field];
        try {
          // Only an admin can edit a post author
          if ((field == "author" && !req.user.isAdmin) || field == "_id") {
            continue;
          }
          // If the new value is null, the field is deleted in the DB to save storage space
          if (newValue == "" || newValue == "undefined" || newValue == null) {
            if (!(field == "title" || field == "author")) {
              res.post.set(field, undefined);
            }
          } else if (res.post.get(field) !== newValue) {
            res.post.set(field, newValue);
          }
        } catch (e) {
          console.log(e);
          return res.status(400).json({ error: `Server error : ${e.message}` });
        }
      }
      const updatedPost = await res.post.save();
      res.json(updatedPost);
    } catch (e) {
      return res.status(500).json({ error: `Server error : ${e.message}` });
    }
  } else {
    return res.status(403).json({ error: "Access forbidden" });
  }
});

/**
 * If req.user is admin or the owner of the post :post, delete the post and return nothing (204)
 * If req.user is someone else, returns unauthorized (403)
 */
router.delete("/:post", authToken, getPost, async (req, res) => {
  if (req.user.isAdmin || res.post.author._id == req.user.id) {
    try {
      await User.updateMany({ subscribedTo: req.params.post }, { $pull: { subscribedTo: req.params.post } });
      // await Tag.updateMany({ relatedPosts: req.params.post }, { $pull: { relatedPosts: req.params.post } });
      await Comment.deleteMany({ originalPost: req.params.post });
      await res.post.deleteOne();
      return res.status(204).json();
    } catch (e) {
      return res.status(500).json({ error: `Server error : ${e.message}` });
    }
  } else {
    return res.status(403).json({ error: "Access forbidden" });
  }
});

/**
 * Gets and returns a post by his id
 * If the post is not stored in the DB, returns 404 Not found
 */
export async function getPost(req, res, next) {
  let post;
  try {
    if (!ObjectId.isValid(req.params.post)) return res.status(404).json({ error: "Post type not ObjectId" });
    post = await Post.findById(req.params.post).populate([
      //   { path: "subscribers", select: { username: 1, _id: 0 } },
      { path: "tags", select: { title: 1 } },
      { path: "author", select: { username: 1 } },
    ]);
    if (post == null) {
      return res.status(404).json({ error: "Post not found" });
    }
  } catch (e) {
    return res.status(500).json({ error: `Server error : ${e.message}` });
  }
  res.post = post;
  next();
}

export default router;
