import { Router } from "express";
const { authToken } = require("../utils/tokenCheck");
const bcrypt = require("bcryptjs");

import User from "../models/user";
import Post from "../models/post";
import Tag from "../models/tag";
import { getPost } from "./posts";
import { getTag } from "./tags";
import { formatUser, formatUserPublic } from "../utils/objectFormatter";

const router = Router();

/**
 * If req.user is admin, returns all users
 * If req.user is someone else, returns unauthorized (403)
 */
router.get("/", authToken, async (req, res) => {
  if (req.user.isAdmin) {
    try {
      let users = await User.find();
      return res.json({ users: users });
    } catch (e) {
      return res.status(500).json({ error: `Server error : ${e.message}` });
    }
  } else {
    return res.status(403).json({ error: "Access forbidden" });
  }
});

router.patch("/:username/subscribe/:post", authToken, getUser, getPost, async (req, res) => {
  if (!req.body.action) return res.status(400).json({ error: "Need json with action field" });
  try {
    if (req.body.action === "subscribe") {
      if (res.user.subscribedTo.includes(res.post.id) || res.user.subscribedTo.some((e) => e.id === res.post.id))
        return res.status(409).json({ error: "Already subscribed" });
      // res.user.subscribedTo.push(res.post.id);
      // res.user.save();
      // res.post.subscribers.push(res.user.id);
      // res.post.save();
      await User.updateOne({ _id: res.user._id }, { $push: { subscribedTo: res.post.id } });
      await Post.updateOne({ _id: res.post._id }, { $push: { subscribers: res.user.id } });
      return res.json({ status: "subscribed" });
    } else if (req.body.action === "unsubscribe") {
      if (!(res.user.subscribedTo.includes(res.post.id) || res.user.subscribedTo.some((e) => e.id === res.post.id)))
        return res.status(409).json({ error: "Already unsubscribed" });
      // res.user.subscribedTo.pull(res.post.id);
      // res.user.save();
      // res.post.subscribers.pull(res.user.id);
      // res.post.save();
      await User.updateOne({ _id: res.user._id }, { $pull: { subscribedTo: res.post.id } });
      await Post.updateOne({ _id: res.post._id }, { $pull: { subscribers: res.user.id } });
      return res.json({ status: "unsubscribed" });
    } else return res.status(400).json({ error: "Action field must be 'subscribe' or 'unsubscribe'" });
  } catch (e) {
    console.log(e);
    return res.status(500).json({ error: `Server error : ${e.message}` });
  }
});

// Not implemented yet
router.patch("/:username/tag/:tag", authToken, getUser, getTag, async (req, res) => {
  if (!req.body.action) return res.status(400).json({ error: "Need json with action field" });
  try {
    if (req.body.action === "subscribe") {
      if (res.user.tags.includes(res.tag.id) || res.user.tags.some((e) => e.id === res.tag.id))
        return res.status(409).json({ error: "Already subscribed" });
      // res.user.tags.push(res.tag.id);
      // res.user.save();
      // res.tag.subscribers.push(res.user.id);
      // res.tag.save();
      await User.updateOne({ _id: res.user._id }, { $push: { tags: res.tag.id } });
      await Tag.updateOne({ _id: res.tag._id }, { $push: { subscribers: res.user.id } });
      return res.json({ status: "subscribed" });
    } else if (req.body.action === "unsubscribe") {
      if (!(res.user.tags.includes(res.tag.id) || res.user.tags.some((e) => e.id === res.tag.id)))
        return res.status(409).json({ error: "Already unsubscribed" });
      // res.user.tags.pull(res.tag.id);
      // res.user.save();
      // res.tag.subscribers.pull(res.user.id);
      // res.tag.save();
      await User.updateOne({ _id: res.user._id }, { $pull: { tags: res.tag.id } });
      await Tag.updateOne({ _id: res.tag._id }, { $pull: { subscribers: res.user.id } });
      return res.json({ status: "unsubscribed" });
    } else return res.status(400).json({ error: "Action field must be 'subscribe' or 'unsubscribe'" });
  } catch (e) {
    console.log(e);
    return res.status(500).json({ error: `Server error : ${e.message}` });
  }
});

/**
 * If req.user is admin, returns the user :username full data
 * If req.user is :username, returns the user :username full data except isAdmin & tempEmailToken
 * If req.user is someone else, returns the user :username formatted (only returns username, profile pic and tags)
 */
router.get("/:username", authToken, getUser, async (req, res) => {
  if (req.user.isAdmin) {
    return res.json({ user: res.user });
  } else if (req.params.username == req.user.username) {
    return res.json({ user: formatUser(res.user) });
  } else {
    return res.json({ user: formatUserPublic(res.user) });
  }
});

/**
 * If req.user is admin or :username, patches the user :username on updated fields and returns the user with the new values
 *    If the value in the body is null or empty, it deletes the field in the DB
 *    If the value is different from the current one in the DB, it updates the field with the new value
 *    If the value is the same as the one in the DB, or if the field is not part of the body, the field is not updated
 * If req.user is someone else, returns unauthorized (403)
 */
router.patch("/:username", authToken, getUser, async (req, res) => {
  if (req.user.isAdmin || req.params.username == req.user.username) {
    try {
      // For each field, checks if it is different from the current value and is not empty
      for (let field in req.body) {
        let newValue = req.body[field];
        try {
          // Only an admin can edit a user username and set a user admin
          if ((field == "username" || field == "isAdmin") && !req.user.isAdmin) {
            continue;
          }
          // If the new value is null, the field is deleted in the DB to save storage space
          if (newValue == "" || newValue == "undefined" || newValue == null) {
            if (!(field == "password" || field == "email" || field == "tempEmail" || field == "username")) {
              res.user.set(field, undefined);
            }
          } else if (
            res.user.get(field) !== newValue
            // (res.user.get(field) !== newValue && field !== "tempEmail") ||
            // (field == "tempEmail" && newValue !== res.user.get("email"))
          ) {
            if (field == "password") {
              // Hash of the new password
              const salt = await bcrypt.genSalt(10);
              newValue = await bcrypt.hash(req.body.password, salt);
            }
            // Confirm temporary email, to implement later
            /**else if (field == "tempEmail") {
              let userAlreadyExists = await User.findOne({ email: newValue });
              if (!userAlreadyExists) {
                let link = idGen(20);
                res.user.set(field, newValue);
                res.user.set("tempEmailToken", link);
                await sendMail(newValue, { type: "editEmail", link, username: res.user.username });
              } else {
                return res.status(401).json({ error: "Email address already used by another account." });
              }
              continue;
            } else if (field == "email") continue;**/
            res.user.set(field, newValue);
          }
        } catch (e) {
          console.log(e);
          res.status(400).json({ error: e.message });
        }
      }
      const updatedUser = await User.findOneAndUpdate({ username: res.user.username }, res.user, {
        new: true,
        useFindAndModify: false,
      });
      res.json(updatedUser);
    } catch (e) {
      return res.status(500).json({ error: `Server error : ${e.message}` });
    }
  } else {
    return res.status(403).json({ error: "Access forbidden" });
  }
});

/**
 * If req.user is admin or :username, delete the user and return nothing (204)
 * If req.user is someone else, returns unauthorized (403)
 */
router.delete("/:username", authToken, getUser, async (req, res) => {
  if (req.user.isAdmin || req.params.username == req.user.username) {
    try {
      await User.deleteOne({ username: req.params.username });
      return res.status(204).json();
    } catch (e) {
      return res.status(500).json({ error: `Server error : ${e.message}` });
    }
  } else {
    return res.status(403).json({ error: "Access forbidden" });
  }
});

/**
 * If req.user is admin or :username, checks if the password provided in the body is the same as the one stored in the user
 * If req.user is someone else, returns unauthorized (403)
 */
router.post("/checkPassword/:username", authToken, getUser, async (req, res) => {
  if (req.user.isAdmin || req.params.username == req.user.username) {
    try {
      if (!(await bcrypt.compare(req.body.password, res.user.password))) return res.json({ password: false });
      return res.json({ password: true });
    } catch (e) {
      return res.status(500).json({ error: `Server error : ${e.message}` });
    }
  } else {
    return res.status(403).json({ error: "Access forbidden" });
  }
});

/**
 * Gets and returns a user by his username
 * If the username is not stored in the DB, returns 404 Not found
 */
export async function getUser(req, res, next) {
  let user;
  try {
    user = await User.findOne({ username: req.params.username });
    // .populate([
    //   { path: "subscribedTo", select: "title content tags author" },
    //   { path: "tags", select: "title" },
    // ]);
    if (user == null) {
      return res.status(404).json({ error: "User not found" });
    }
  } catch (e) {
    return res.status(500).json({ error: `Server error : ${e.message}` });
  }
  res.user = user;
  next();
}

export default router;
