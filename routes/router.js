import login from "./login";
import register from "./register";
import users from "./users";
import posts from "./posts";
import comments from "./comments";
import tags from "./tags";
import notifications from "./notifications";

export default {
  users,
  login,
  register,
  posts,
  comments,
  tags,
  notifications,
};
