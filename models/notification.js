const mongoose = require("mongoose");

const notificationSchema = new mongoose.Schema(
  {
    target: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    content: {
      type: String,
      required: true,
      default: "Nouvelle notification",
    },
    title: {
      type: String,
    },
    link: {
      type: String,
    },
    sender: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    tags: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Tag",
      },
    ],
  },
  { timestamps: { createdAt: "createdAt", updatedAt: false } },
  { versionKey: false }
);

module.exports = mongoose.model("Notification", notificationSchema);
