const mongoose = require("mongoose");

const userSchema = new mongoose.Schema(
  {
    isAdmin: {
      type: Boolean,
    },
    username: {
      type: String,
      required: true,
    },
    password: {
      type: String,
      required: true,
    },
    email: {
      type: String,
    },
    tags: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Tag",
      },
    ],
    tempEmail: {
      type: String,
    },
    tempEmailToken: {
      type: String,
    },
    tempPasswordToken: {
      type: String,
    },
    subscribedTo: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Post",
      },
    ],
  },
  { versionKey: false }
);

module.exports = mongoose.model("User", userSchema);
