const mongoose = require("mongoose");

const { notifyOnNewPost } = require("../routes/notifications");

const postSchema = new mongoose.Schema(
  {
    title: {
      type: String,
      required: true,
    },
    author: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
    },
    content: {
      type: String,
      required: true,
    },
    tags: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Tag",
      },
    ],
    subscribers: [
      {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
      },
    ],
  },
  { timestamps: { createdAt: "createdAt", updatedAt: "updatedAt" } },
  { versionKey: false }
);

// Notify all tag's subscribers when a new idea is posted with tags
postSchema.post("save", async (doc) => {
  await notifyOnNewPost(doc);
});

module.exports = mongoose.model("Post", postSchema);
