const mongoose = require("mongoose");

const { notifyOnNewComment } = require("../routes/notifications");

const commentSchema = new mongoose.Schema(
  {
    author: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "User",
      required: true,
    },
    content: {
      type: String,
      required: true,
    },
    originalPost: {
      type: mongoose.Schema.Types.ObjectId,
      ref: "Post",
      required: true,
    },
  },
  { timestamps: { createdAt: "createdAt", updatedAt: "updatedAt" } },
  { versionKey: false }
);

// Notify the original post owner and all subscribed users when a new comment is sent
commentSchema.post("save", async (doc) => {
  await notifyOnNewComment(doc);
});

module.exports = mongoose.model("Comment", commentSchema);
