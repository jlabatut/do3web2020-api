import express from "express";
import mongoose from "mongoose";
const cors = require("cors");
const fs = require("fs");

import "dotenv/config";
const PORT = process.env.PORT || 3000;
const DB_NAME = process.env.DB_NAME || "doproject";
const DB_URL = process.env.DB_URL || "mongodb://localhost";

import router from "./routes/router";

const app = express();
const connectionOptions = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  dbName: DB_NAME,
};

mongoose.connect(DB_URL, connectionOptions);
const db = mongoose.connection;
db.on("error", (e) => console.error(e));

app.use(express.json());
app.use(cors());

app.use((req, res, next) => {
  const url = req.url;
  let text = `${req.socket.remoteAddress} ${new Date().toString().substr(4, 20)} ${req.method} ${url} `;
  res.on("finish", () => {
    text += `| ${res.statusCode} ${res.getHeader("content-length")}`;
    fs.appendFile("app.log", `${text}\n`, function (err) {
      if (err) throw err;
    });
    if (res.statusCode >= 400)
      console.log(`Error ${res.statusCode} on ${req.method} ${url} by ${req.socket.remoteAddress}`);
  });
  next();
});

app.use("/login", router.login);
app.use("/register", router.register);
app.use("/users", router.users);
app.use("/posts", router.posts);
app.use("/comments", router.comments);
app.use("/tags", router.tags);
app.use("/notifications", router.notifications);

app.get("/", (req, res) => {
  res.json({ data: "Hello World!" });
});

app.get("/*", (req, res) => {
  res.status(404).json({ error: "Page not found" });
});

const server = app.listen(PORT);

const gracefulShutdown = () => {
  server.close(() => {
    console.log("Backend closed.");
    // Bug if using mongoose disconnect
    process.exit(0);
  });
};

process.on("SIGINT", gracefulShutdown);
process.on("SIGTERM", gracefulShutdown);
